from bgez.framework.managers import EntityManager
from bgez.framework.managers import InputManager
from bgez.framework.types import CameraObject

from bgez.framework.objects import Mapping
from bgez.framework.objects import Input
from bgez.framework.inputs import AxisMapping, MouseAxis
from bgez.framework.inputs import ButtonMapping, MouseButton, KeyboardButton

from bgez import Utils

from mathutils import Vector

@EntityManager.LinkToObjects('Camera')
class Camera(CameraObject):

    SPEED = 0.5

    TELEPORT_INCREMENT = 5

    Controls = Input(InputManager, 'MainControls') \
        + AxisMapping('x', MouseAxis('x')) \
        + AxisMapping('y', MouseAxis('y')) \
        + ButtonMapping('forward',
            KeyboardButton(Input.Keys.WKEY),
            KeyboardButton(Input.Keys.ZKEY)) \
        + ButtonMapping('backward',
            KeyboardButton(Input.Keys.SKEY)) \
        + ButtonMapping('left',
            KeyboardButton(Input.Keys.AKEY),
            KeyboardButton(Input.Keys.QKEY)) \
        + ButtonMapping('right',
            KeyboardButton(Input.Keys.DKEY)) \
        + ButtonMapping('up',
            KeyboardButton(Input.Keys.SPACEKEY)) \
        + ButtonMapping('down',
            KeyboardButton(Input.Keys.LEFTSHIFTKEY)) \
        + ButtonMapping('teleport',
            KeyboardButton(Input.Keys.LEFTCTRLKEY, 'ONCEON')) \
        + ButtonMapping('wheelup',
            MouseButton(Input.Keys.WHEELUPMOUSE, 'ONCEON', 'ON')) \
        + ButtonMapping('wheeldown',
            MouseButton(Input.Keys.WHEELDOWNMOUSE, 'ONCEON', 'ON')) \
        ;

    @Mapping.bind('x', 'y')
    def look(self, data={}):
        self.applyRotation((0, 0, data['x']), False)
        self.applyRotation((data['y'], 0, 0), True)

    @Mapping.bind('forward', 'backward', 'left', 'right')
    def movement(self, data={}):
        m = Vector((0, 0, 0))

        d = {
            'forward':  (0, 0, -1),
            'backward': (0, 0, 1),
            'left':     (-1, 0, 0),
            'right':    (1, 0, 0),
        }

        for key in data:
            if key in d:
                m += Vector(d[key])
        m.normalize()

        self.applyMovement(self.SPEED * m, True)

    @Mapping.bind('up', 'down')
    def altitude(self, data={}):
        self.setLinearVelocity((0, 0, 0))
        self.applyMovement(
            (0, 0, self.SPEED * (int('up' in data) - int('down' in data)))
        )

    @Mapping.teleport
    def teleport(self, data={}):
        self.setLinearVelocity((0, 0, 0))
        self.worldPosition.z += self.TELEPORT_INCREMENT

    @Mapping.bind('wheelup', 'wheeldown')
    def waterLevel(self, data={}):
        waterLevel = Utils.getObject('WaterLevel')
        if waterLevel:
            waterLevel.applyMovement(
                (0, 0, (int('wheelup' in data) - int('wheeldown' in data)))
            )

            for waterTile in (o for o in self.scene.objects \
            if o.name.startswith('Water')):
                waterTile.worldPosition.z = waterLevel.worldPosition.z
