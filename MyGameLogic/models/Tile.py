from bgez.framework.baseclasses import IniDict
from bgez.framework.types import Asset, GameObject
from bgez import Utils
from bgez import Log

import bgez
import time
import random
import weakref

from mathutils import noise
from mathutils import kdtree
from mathutils import geometry

from mathutils import Vector

from collections import namedtuple
from collections import OrderedDict

Position = namedtuple('Position', ('x', 'y'))

class Tile(Asset, GameObject):
    ObjectName = 'Tile1'

    # Extra
    WATER_NAME = 'Water1'

    # Offset param
    RAND_RANGE = 1 << 16

    # Perlin noise params
    HEIGHT = 10
    FLOOR = -4
    SCALE = 0.05

    # Color mapping
    COLOR_DEFAULT = (.9, .8, .5, 1)
    COLOR_HEIGHTS = OrderedDict()
    COLOR_HEIGHTS[15] = (1, 1, 1, 1)
    COLOR_HEIGHTS[6] = (.7, .7, .7, 1)
    COLOR_HEIGHTS[2] = (.1, .8, .1, 1)

    # Parameters for the KDTree
    MAX_VERTICES = 2048
    FIND_RANGE = 0.01

    # Debug info
    NORMALS = True
    SMOOTH = True

    TileCount = 0

    @classmethod
    def classinit(cls):
        super().classinit()
        """This is the virtual grid register. Slots are empty by default"""
        cls.Registry = weakref.WeakValueDictionary()

    def construct(self):
        self.n = self.__class__.TileCount
        self.__class__.TileCount += 1

        self.libName = '{}{}'.format(Utils.getQualName(self.__class__), self.n)
        self.updatedPhysics = False
        self.gridPosition = None
        self.vertices = []
        self.KDTree = None

    def cloneMesh(self):
        """Creates a new UNIQUE mesh"""
        new = bgez.logic.LibNew(
            self.libName, 'Mesh', [self.meshes[0].name]
        )[0]
        self.replaceMesh(new)
        self.meshes[0] = new
        return new

    def deform(self, neighbors, seed):
        mesh = self.cloneMesh()

        randgen = random.Random()
        randgen.seed(seed)
        offset = Vector((randgen.random() for _ in range(3))) * randgen.random()
        offset *= self.RAND_RANGE

        passes = (
            (1., 1.2, .6, 4.),
            # (.5, 1., 1.5, 3.),
            # (.01, 1., 1.5, 3.),
        )

        # Lookup kdtree
        self.KDTree = kdtree.KDTree(self.MAX_VERTICES)
        self.vertices.clear()
        VID = 0

        # Moving the vertices
        for materialId in range(len(mesh.materials)):
            for vertexId in range(mesh.getVertexArrayLength(materialId)):
                vertex = mesh.getVertex(materialId, vertexId)
                vpos = self.worldTransform * vertex.XYZ
                vpos.z = self.GetHeight((offset + vpos) * self.SCALE, passes) \
                    * (self.HEIGHT - self.FLOOR) + self.FLOOR
                vertex.XYZ = self.worldTransform.inverted() * vpos
                vertex.color = self.GetColor(vpos.z)

                # Inserting the vertex in the KDTree
                self.vertices.append(vertex)
                self.KDTree.insert(vpos, VID)
                VID += 1

        # Update the KDTree
        self.KDTree.balance()

        if self.NORMALS: # Calculating normals and storing polygons
            for polygonId in range(mesh.numPolygons):
                polygon = mesh.getPolygon(polygonId)
                v1 = mesh.getVertex(0, polygon.v1)
                v2 = mesh.getVertex(0, polygon.v2)
                v3 = mesh.getVertex(0, polygon.v3)
                v4 = mesh.getVertex(0, polygon.v4)
                normal = geometry.normal(
                    *tuple(vertex.XYZ for vertex in (v1, v2, v3, v4)))
                v1.normal = normal
                v2.normal = normal
                v3.normal = normal
                v4.normal = normal

        if self.SMOOTH: # Normals smoothing between neighbors
            for neighbor in neighbors:
                for vertex in self.vertices:
                    pos = self.worldTransform * vertex.XYZ
                    for npos, nid, nd in neighbor.KDTree.find_range(
                    pos, self.FIND_RANGE):
                        neighborVertex = neighbor.vertices[nid]
                        normal = (vertex.normal + neighborVertex.normal) * .5
                        neighborVertex.normal = normal
                        vertex.normal = normal

        # Updating visuals (sometimes it does not refresh...)
        self.replaceMesh(mesh, True, False)

    def updatePhysics(self):
        self.reinstancePhysicsMesh(self, self.meshes[0], 1)
        self.updatedPhysics = True

    def destroy(self):
        try:
            bgez.logic.LibFree(self.libName)
        finally:
            self.endObject()

    @classmethod
    def GetHeight(cls, pos, passes):
        """#https://github.com/martijnberger/blender/blob/c359343f8dae6689c955dc1fa700cb26f6cd2e95/source/blender/blenlib/intern/noise.c#L1562

        float rmd, value = 0.0, pwr = 1.0, pwHL = powf(lacunarity, -H);

        for (i = 0; i < (int)octaves; i++) {
            value += noisefunc(x, y, z) * pwr;
            pwr *= pwHL;
            x *= lacunarity;
            y *= lacunarity;
            z *= lacunarity;
        }

        rmd = octaves - floorf(octaves);
        if (rmd != 0.f) value += rmd * noisefunc(x, y, z) * pwr;
        #"""

        z = 0
        for factor, *args in passes:
            z += noise.fractal(factor * pos, *args)
        return .5 * z / len(passes) + .5

    @classmethod
    def GetColor(cls, height):
        for h, color in cls.COLOR_HEIGHTS.items():
            if height > h:
                return color
        return cls.COLOR_DEFAULT
